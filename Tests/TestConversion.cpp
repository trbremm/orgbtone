#include "stdafx.h"
#include "CppUnitTest.h"
#include <cmath>
#include <stdio.h>
#include <wchar.h>
#include "../oRGBSpace/oRGB.h"

#define DELTA_TOLERANCE 0.025

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace Tests
{		
	TEST_CLASS(UnitTest1)
	{
	public:

		static void AssertEqualPixels(const double* a, const double* b) {
			bool are_equal = true;
			for (unsigned i = 0; i < 3; ++i) {
				if (std::abs(a[i] - b[i]) > DELTA_TOLERANCE) {
					are_equal = false;
					break;
				}
			}
			
			wchar_t msg[100] = {};
			swprintf(msg, sizeof msg, L"[%f, %f, %f] != [%f, %f, %f]", a[0], a[1], a[2], b[0], b[1], b[2]);
			Assert::IsTrue(are_equal, msg);
		}

		static void PrintPixel(const double* pixel) {
			char msg[100] = {};
			sprintf_s(msg, "[%f, %f, %f]", pixel[0], pixel[1], pixel[2]);
		}

		static void AssertPixelConvertionValid(const double* pixel_rgb) {
			double pixel_orgb[3]{};
			double convert_back_result[3]{};

			oRGB::convertFromRGB(pixel_rgb, pixel_orgb);
			oRGB::convertToRGB(pixel_orgb, convert_back_result);

			AssertEqualPixels(pixel_rgb, convert_back_result);
		}
		
		TEST_METHOD(TestRGBTooRGBAndBack)
		{

			const double white[] {1.0, 1.0, 1.0};
			AssertPixelConvertionValid(white);

			const double black[]{ 0.0, 0.0, 0.0 };
			AssertPixelConvertionValid(black);

			const double red[] { 1.0, 0.0, 0.0 };
			AssertPixelConvertionValid(red);

			const double green[] { 0.0, 1.0, 0.0 };
			AssertPixelConvertionValid(green);

			const double blue[] { 0.0, 0.0, 1.0 };
			AssertPixelConvertionValid(blue);

			const double yellow[]{ 1.0, 1.0, 0.0 };
			AssertPixelConvertionValid(yellow);

			const double magenta[]{ 1.0, 0.0, 1.0 };
			AssertPixelConvertionValid(magenta);

			const double cyan[]{ 0.0, 1.0, 1.0 };
			AssertPixelConvertionValid(cyan);

			const double salmon[] { 250.0 / 255.0, 128.0 / 255.0, 114.0 / 255.0 };
			AssertPixelConvertionValid(salmon);

			const double violet[] { 238 / 255.0, 130 / 255.0, 238 / 255.0 };
			AssertPixelConvertionValid(violet);

			const double indigo[] { 75 / 255.0, 0 / 255.0, 130 / 255.0 };
			AssertPixelConvertionValid(indigo);

			const double forest_green[] { 34 / 255.0, 139 / 255.0, 34 / 255.0 };
			AssertPixelConvertionValid(forest_green);

			const double orange[] { 255 / 255.0, 16 / 255.0, 0 / 255.0 };
			AssertPixelConvertionValid(orange);
		}

		TEST_METHOD(TestNormalizePixel)
		{

			const uchar bytes[]{ 255, 0, 230, 25 };
			double normalized[4] = {};
			uchar back_to_bytes[4] = {};

			oRGB::normalizeUCharToDouble(bytes, normalized, sizeof bytes);
			oRGB::fromNormalizedDoubleToUChar(normalized, back_to_bytes, sizeof bytes);

			for (unsigned i = 0; i < 4; ++i) {
				Assert::AreEqual(back_to_bytes[i], bytes[i], DELTA_TOLERANCE);
			}
		}

		TEST_METHOD(TestNormalizePixelRespectsGamut)
		{

			double normalized[4] = {1.5, -0.5, 0.5};
			uchar expected[4] = { 255, 0, 127 };
			uchar back_to_bytes[4] = {};

			oRGB::fromNormalizedDoubleToUChar(normalized, back_to_bytes, sizeof normalized);

			for (unsigned i = 0; i < 4; ++i) {
				Assert::AreEqual(back_to_bytes[i], expected[i]);
			}
		}

	};
}