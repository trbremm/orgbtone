#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_oRGBTone.h"
#include <QtWidgets/QLabel>

class oRGBTone : public QMainWindow
{
	Q_OBJECT

public:
	oRGBTone(QWidget *parent = Q_NULLPTR);

protected:
	void onOpenFileClicked();
	void onSaveToClicked();
	void processImage();
	void updateView();

private:
	Ui::oRGBToneClass ui;
	QImage sourceImage;
	QImage gridImage;
	
	const double cyb_step = 0.1;
	const double crg_step = 0.1;
};
