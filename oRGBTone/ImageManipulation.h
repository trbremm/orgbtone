#pragma once
#include <qimage.h>

class ImageManipulation
{
public:
	static QImage processImage(QImage& image, const double cyb_shift = 0.0,
		const double crg_shift = 0.0);

	static QImage createToneShiftedGridFromImage(QImage& image, 
		const double cyb_shift = 0.0,
		const double crg_shift = 0.0,
		int dimensions = 3);
};

