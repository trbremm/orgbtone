#include "ImageManipulation.h"

#include "../oRGBSpace/oRGB.h"
#include <qpainter.h>

QImage ImageManipulation::processImage(QImage & image, const double cyb, 
	const double crg)
{
	if (image.isNull()) {
		return QImage();
	}

	size_t image_size = image.sizeInBytes();
	const QRgb* data = reinterpret_cast<const QRgb*>(image.constBits());
	std::shared_ptr<uchar> bits_ptr = createSharedArray<uchar>(image_size);
	uchar* bits = bits_ptr.get();

	for (size_t i = 0, j = 0; i < image_size; i += 4, ++j) {
		bits[i + 0] = qAlpha(data[j]);
		bits[i + 1] = qRed(data[j]);
		bits[i + 2] = qGreen(data[j]);
		bits[i + 3] = qBlue(data[j]);
	}

	std::shared_ptr<uchar> converted_ptr = createSharedArray<uchar>(image_size);
	uchar* converted_back = converted_ptr.get();

	oRGB::changeTone(bits, converted_back, image_size, cyb, crg);

	QImage result(image.size(), image.format());
	QRgb* result_data = reinterpret_cast<QRgb*>(result.bits());

	for (size_t i = 0, j = 0; i < image_size; i += 4, ++j) {
		result_data[j] = qRgba(
			converted_back[i + 1],
			converted_back[i + 2],
			converted_back[i + 3],
			qAlpha(data[j]));
	}

	return result;
}

QImage ImageManipulation::createToneShiftedGridFromImage(QImage & image, 
	const double cyb_shift_step, const double crg_shift_step, int dimensions)
{
	QSize grid_size(image.size());
	grid_size *= dimensions;
	int cell_width = image.width();
	int cell_height = image.height();

	QImage grid(grid_size, image.format());
	QPainter painter(&grid);
	
	double cyb_shift = -cyb_shift_step * dimensions / 2;
	 
	for (int x = 0; x < grid_size.width(); x += cell_width) {
		double crg_shift = crg_shift_step * dimensions / 2;

		for (int y = 0; y < grid_size.width(); y += cell_height) {
			QImage cell = processImage(image, cyb_shift, crg_shift);
			painter.drawImage(x, y, cell);
			crg_shift -= crg_shift_step;
		}
		cyb_shift += cyb_shift_step;
	}

	return grid;
}
