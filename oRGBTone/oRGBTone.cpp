#include "oRGBTone.h"
#include "ImageManipulation.h"

#include <qpushbutton.h>
#include <qfiledialog.h>
#include <qmessagebox.h>
#include <qlabel.h>
#include <qcolor.h>
#include <qslider.h>
#include <qrgb.h>
#include <qevent.h>

oRGBTone::oRGBTone(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);

	connect(ui.openFileButton, &QPushButton::clicked,
		this, &oRGBTone::onOpenFileClicked);
	connect(ui.saveToButton, &QPushButton::clicked,
		this, &oRGBTone::onSaveToClicked);
}

void oRGBTone::onOpenFileClicked()
{
	QString file_name = QFileDialog::getOpenFileName(this, "Open Image", "", 
		"Image Files (*.png *.jpg *.bmp *.gif)");

	if (file_name.isNull()) {
		return;
	}

	sourceImage.load(file_name);
	if (sourceImage.isNull()) {
		QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
			"Unable to load image.");
		return;
	}
	sourceImage = sourceImage.convertToFormat(QImage::Format_ARGB32);
	processImage();
}

void oRGBTone::onSaveToClicked()
{
	QString file_name = QFileDialog::getSaveFileName(this, "Save to", "",
		"Image Files (*.png *.jpg *.bmp *.gif)");

	if (file_name.isNull()) {
		return;
	}

	QImage result = ImageManipulation::createToneShiftedGridFromImage(
		sourceImage, cyb_step, crg_step);

	if (result.isNull() || !result.save(file_name)) {
		QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
			"Unable to save image.");
		return;
	}
}

void oRGBTone::processImage()
{
	if (sourceImage.isNull()) {
		return;
	}
	
	gridImage = ImageManipulation::createToneShiftedGridFromImage(
		sourceImage, cyb_step, crg_step);

	if (gridImage.isNull()) {
		QMessageBox::information(this, QGuiApplication::applicationDisplayName(),
			"Error when converting image.");
		return;
	}
	updateView();
}

void oRGBTone::updateView()
{
	if (gridImage.isNull()) {
		return;
	}
	int w = ui.imageView->width() - 5;
	int h = ui.imageView->height() - 5;
	ui.imageView->setPixmap(QPixmap::fromImage(gridImage).scaled(w, h, Qt::KeepAspectRatio));
}
