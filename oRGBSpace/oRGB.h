#pragma once
typedef unsigned char uchar;

#include <memory>

template<typename T>
std::shared_ptr<T> createSharedArray(size_t size) {
	return std::shared_ptr<T>(new T[size], std::default_delete<T[]>());
}

class oRGB
{
public:

	static void convertFromRGB(const double* from_rgb, double* to_orgb);

	static void convertToRGB(const double* from_orgb, double* to_rgb);

	static void normalizeUCharToDouble(const uchar* bytes, double* value,
		size_t size);

	static void fromNormalizedDoubleToUChar(const double* value, uchar* bytes,
		size_t size);

	static void changeTone(const uchar* bits, uchar* converted_back, 
		size_t size, const double cyb=0.0, const double crg=0.0);
};

