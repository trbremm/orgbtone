#include "oRGB.h"

#include <cstring>
#include <cmath> 
#include <algorithm>

#define PI_VALUE 3.14159265358979323846
#define PIXEL_VALUE 255.0
#define BYTES_PER_PIXEL 4

double truncateToRange(const double value, const double upper=1.0, const double lower=-1.0) {
	return std::min(std::max(value, lower), upper);
}

inline void multiplyMatrix3x3Vector3D(const double* matrix, const double* vector, double* result) 
{
	result[0] = matrix[0] * vector[0] + matrix[1] * vector[1] + matrix[2] * vector[2];
	result[1] = matrix[3] * vector[0] + matrix[4] * vector[1] + matrix[5] * vector[2];
	result[2] = matrix[6] * vector[0] + matrix[7] * vector[1] + matrix[8] * vector[2];
}

void oRGB::convertFromRGB(const double* from_rgb, double* to_orgb) 
{
	const double rgb_lcc_transf[] = { 
		0.2990, 0.5870, 0.1140,
		0.5000, 0.5000, -1.000,
		0.8660, -0.8660, 0.0000 };

	// Linear transformation from RGB to L'C'C'
	double lcc[3];
	multiplyMatrix3x3Vector3D(rgb_lcc_transf, from_rgb, lcc);

	// Rotate L'C'C' to oRGB
	const double rgb_angle = atan2(lcc[2], lcc[1]);
	const double abs_rgb_angle = abs(rgb_angle);
	const double sign = rgb_angle >= 0.0 ? 1.0 : -1.0;
	const double orgb_angle = abs_rgb_angle < PI_VALUE / 3.0 ?
		3.0 / 2.0 * rgb_angle : 
		(PI_VALUE / 2.0 + 3.0 / 4.0 * (abs_rgb_angle - PI_VALUE / 3.0)) * sign;
	const double rotation_angle = orgb_angle - rgb_angle;
	const double cos_val = cos(rotation_angle);
	const double sin_val = sin(rotation_angle);
	const double l_rotation[] = {
		1.0,    0.0,     0.0,
		0.0, cos_val,  -sin_val,
		0.0, sin_val,  cos_val};

	multiplyMatrix3x3Vector3D(l_rotation, lcc, to_orgb);
}

void oRGB::convertToRGB(const double* from_orgb, double* to_rgb) {

	// Rotate oRGB to L'C'C'
	const double orgb_angle = atan2(from_orgb[2], from_orgb[1]);
	const double abs_orgb_angle = abs(orgb_angle);
	const double sign = orgb_angle >= 0.0 ? 1.0 : -1.0;
	const double rgb_angle = abs_orgb_angle < PI_VALUE / 2.0 ?
		2.0 / 3.0 * orgb_angle :
		(PI_VALUE / 3.0 + 4.0 / 3.0 * (abs_orgb_angle - PI_VALUE / 2.0)) * sign;
	const double rotation_angle = orgb_angle - rgb_angle;
	const double cos_val = cos(rotation_angle);
	const double sin_val = sin(rotation_angle);
	const double l_rotation[] = {
		1.0,    0.0,     0.0,
		0.0, cos_val,  sin_val,
		0.0, -sin_val,  cos_val};

	double lcc[3];
	multiplyMatrix3x3Vector3D(l_rotation, from_orgb, lcc);

	// Linear transformation from L'C'C' to RGB
	const double lcc_rgb_transf[] = {
		1.0000, 0.1140, 0.7436,
		1.0000, 0.1140, -0.4111,
		1.0000, -0.8660, 0.1663 };

	multiplyMatrix3x3Vector3D(lcc_rgb_transf, lcc, to_rgb);
}

void oRGB::normalizeUCharToDouble(const uchar* bytes, double* value, size_t size) {
	for (size_t i = 0; i < size; ++i) {
		value[i] = bytes[i] / PIXEL_VALUE;
	}
}

void oRGB::fromNormalizedDoubleToUChar(const double * value, uchar * bytes, size_t size)
{
	for (size_t i = 0; i < size; ++i) {
		bytes[i] = (uchar) (truncateToRange(value[i] * PIXEL_VALUE, 255.0, 0.0));
	}
}

void oRGB::changeTone(const uchar* bits, uchar* converted_back, size_t size, 
	const double cyb_shift, const double crg_shift)
{
	std::shared_ptr<double> pixels_ptr = createSharedArray<double>(size);
	std::shared_ptr<double> orgb_pixels_ptr = createSharedArray<double>(size);
	double* pixels = pixels_ptr.get();
	double* orgb_pixels = orgb_pixels_ptr.get();

	oRGB::normalizeUCharToDouble(bits, pixels, size);
	// ARGB format expected
	for (size_t i = 1; i < size; i += BYTES_PER_PIXEL) {
		oRGB::convertFromRGB(pixels + i, orgb_pixels + i);
		double& cyb = orgb_pixels[i + 1];
		double& crg = orgb_pixels[i + 2];

		crg = truncateToRange(crg + crg_shift);
		cyb = truncateToRange(cyb + cyb_shift);

		oRGB::convertToRGB(orgb_pixels + i, pixels + i);
	}
	
	oRGB::fromNormalizedDoubleToUChar(pixels, converted_back, size);
}

